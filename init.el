;---------------------------------------------------------------------
;Variables d'environnement
;---------------------------------------------------------------------

(defvar home-dir)
(setq home-dir (concat (expand-file-name "~") "/"))

(defvar home-lisp-dir (concat home-dir ".emacs.d/lisp"))
(add-to-list 'load-path home-lisp-dir)

(add-to-list 'load-path (concat home-lisp-dir "/expand-region"))



(let ((default-directory "/usr/share/org-mode/"))
  (normal-top-level-add-subdirs-to-load-path));recursif

(setq custom-file "~/.emacs.d/config/custom.el")
(load custom-file)



(prefer-coding-system 'utf-8)
(set-default-coding-systems 'utf-8)

;; The following lines are always needed.  Choose your own keys.
(global-set-key "\C-cl" 'org-store-link)
(global-set-key "\C-ca" 'org-agenda)
(global-set-key "\C-cb" 'org-iswitchb)
;(add-hook 'org-mode-hook 'turn-on-font-lock) 



;;remember
;(org-remember-insinuate)
(setq org-directory "/data/Donnees/Org")
(setq org-default-notes-file (concat org-directory "/notes.org"))
(define-key global-map "\C-cr" 'org-remember)


(setq c-default-style "bsd"
      c-basic-offset 4)

(defvar homeblogdir "/data/Donnees/Sites/fredtantini/")

(defun newPost (title)
  "open a new file in YEAR/MONTH/YearMonthDay_title_with_underscore.org"
   (interactive "sTitre: ")
  (let* ((tmptitle (replace-regexp-in-string "[ ']" "_" title))
	 (year (format-time-string "%Y"))
	 (month (format-time-string "%m"))
	 (day (format-time-string "%d"))
	 (newtitle (downcase (concat year month day "_" tmptitle ".org")))
	 (newdir (concat homeblogdir year "/" month "/"))
	 )
    (make-directory newdir t)
    (find-file (concat newdir newtitle))
    (insert (concat "#+DATE: <" year "-" month "-" day ">\n"))
    (insert (concat "#+TITLE: " title "\n\n"))
	    
    )
  )

(global-set-key (kbd "<f5>") 'newPost) 


(autoload 'org-drill "org-drill" "orgdrill")

(defun pao ()
  "learn"
  (interactive)
  (find-file "~/.emacs.d/pao.org")
  (org-drill)
  )
(global-set-key (kbd "<f11>") 'pao) 



(global-set-key (kbd "<f2>") 'find-function) 
(global-set-key (kbd "<f9>") 'find-library) 

(scroll-bar-mode 0)
(tool-bar-mode 0)
;(menu-bar-mode 0)


(global-set-key (kbd "<C-wheel-up>") 'text-scale-increase)
(global-set-key (kbd "<C-wheel-down>") 'text-scale-decrease)

(global-set-key (kbd "<C-mouse-4>") 'text-scale-increase)
(global-set-key (kbd "<C-mouse-5>") 'text-scale-decrease)


;;conf org
(load "org-conf.el")

(require 'easy-kill)
(global-set-key [remap kill-ring-save] 'easy-kill)


;;
;; ace jump mode major function
;; 

(autoload 'ace-jump-mode "ace-jump-mode" "Emacs quick move minor mode" t)
;; you can select the key you prefer to
(define-key global-map (kbd "C-é") 'ace-jump-mode)

(autoload 'ace-jump-mode-pop-mark "ace-jump-mode" "Ace jump back" t)
(eval-after-load "ace-jump-mode"
  '(ace-jump-mode-enable-mark-sync))
(define-key global-map (kbd "C-x SPC") 'ace-jump-mode-pop-mark)


(require 'rainbow-delimiters)
(global-rainbow-delimiters-mode)



(require 'package)
(add-to-list 'package-archives
	     '("melpa" . "http://melpa.org/packages/") t)

;;magit

;(add-to-list 'load-path "~/.emacs.d/lisp/magit/lisp")
;(require 'magit)
(global-set-key (kbd "C-x g") 'magit-status)
(global-set-key (kbd "C-x M-g") 'magit-dispatch-popup)

(ido-mode t)
;;;; when C-x b ne demande pas êtes vous sûr si n'existe pas
(setq ido-create-new-buffer 'always)



;;;;;;;;;;;;;;


;;;undo-tree
(add-to-list 'load-path "~/.emacs.d/lisp/undo-tree")
(require 'undo-tree)
(global-undo-tree-mode)


;;;pour emacs-client
(server-start)
(require 'org-protocol)

;;;yasnippet
(add-to-list 'load-path
             "~/.emacs.d/lisp/yasnippet")
(add-to-list 'load-path
             "~/.emacs.d/snippets/yasnippet")
(require 'yasnippet)
(yas-global-mode 1)


;;;company
(add-to-list 'load-path
              "~/.emacs.d/lisp/company")
(require 'company)
(add-hook 'after-init-hook 'global-company-mode)
(global-company-mode 1)

;; company delay until suggestions are shown
(setq company-idle-delay 0.5)
;; weight by frequency
(setq company-transformers '(company-sort-by-occurrence))


;;; yasnippet pour company
;;;;https://www.reddit.com/r/emacs/comments/3r9fic/best_practicestip_for_companymode_andor_yasnippet/

;; Add yasnippet support for all company backends
;; https://github.com/syl20bnr/spacemacs/pull/179
;; (defvar company-mode/enable-yas t "Enable yasnippet for all backends.")
;; 
;; (defun company-mode/backend-with-yas (backend)
;;   (if (or (not company-mode/enable-yas) (and (listp backend)    (member 'company-yasnippet backend)))
;;   backend
;; (append (if (consp backend) backend (list backend))
;;         '(:with company-yasnippet))))
;; 
;; (setq company-backends (mapcar #'company-mode/backend-with-yas company-backends))
;; 



(ido-mode 1)
(ido-everywhere 1)

;;-;; ;;fuzzy matching pour ido
;;-;; ;(add-to-list 'load-path
;;-;; ;             "~/.emacs.d/lisp/flx-master")
;;-;; 
;;-;; ;(require 'flx-ido)
;;-;; 
;;-;; ;(flx-ido-mode 1)
;;-;; ;; disable ido faces to see flx highlights.
;;-;; (setq ido-enable-flex-matching t)
;;-;; (setq ido-use-faces nil)
;;-;; 


;;-;; ;;; helm
;;-;; 
;;-;; ;;(let ((generated-autoload-file (expand-file-name "helm-autoloads.el" "~/.emacs.d/lisp/helm")) \
;;-;; ;;      (backup-inhibited t)) (update-directory-autoloads "~/.emacs.d/lisp/helm"))
;;-;; 
;;-;; 
(add-to-list 'load-path "~/.emacs.d/lisp/async")
(add-to-list 'load-path
             "~/.emacs.d/lisp/helm")
(require 'helm)
(require 'helm-config)

;;-;; ;;;; https://raw.githubusercontent.com/tuhdo/tuhdo.github.io/master/emacs-tutor/helm-intro.org
   (global-set-key (kbd "C-c h") 'helm-command-prefix)
   (global-unset-key (kbd "C-x c"))
 
 ;  (define-key helm-map (kbd "<tab>") 'helm-execute-persistent-action) ; rebind tab to run persistent action
 ;  (define-key helm-map (kbd "C-i") 'helm-execute-persistent-action) ; make TAB works in terminal
 ;  (define-key helm-map (kbd "C-z")  'helm-select-action) ; list actions using C-z
 
   (setq helm-split-window-in-side-p           t ; open helm buffer inside current window, not occupy whole other window
         helm-move-to-line-cycle-in-source     t ; move to end or beginning of source when reaching top or bottom of source.
         helm-ff-search-library-in-sexp        t ; search for library in `require' and `declare-function' sexp.
         helm-scroll-amount                    8 ; scroll 8 lines other window using M-<next>/M-<prior>
         helm-ff-file-name-history-use-recentf t)
 
   (helm-mode 1)
 
 (global-set-key (kbd "M-x") 'helm-M-x)
 ;;  (global-set-key (kbd "M-x") 'execute-extended-command)
 (global-set-key (kbd "M-y") 'helm-show-kill-ring)
 (global-set-key (kbd "C-x b") 'helm-mini)
 (setq helm-buffers-fuzzy-matching t
       helm-recentf-fuzzy-match    t)
 
 (global-set-key (kbd "C-x C-f") 'helm-find-files)
 (global-set-key (kbd "C-c h o") 'helm-occur)
 (global-set-key (kbd "C-h SPC") 'helm-all-mark-rings)
 
 
 


;;; projectile

(add-to-list 'load-path
             "~/.emacs.d/lisp/dash.el") ;projectile require dash
(add-to-list 'load-path
             "~/.emacs.d/lisp/projectile")
(require 'projectile)
(projectile-global-mode)


;;; avec helm-all-mark-rings

(setq projectile-completion-system 'helm)
(require 'helm-projectile)
(helm-projectile-on)

(load "org-plux.el")
