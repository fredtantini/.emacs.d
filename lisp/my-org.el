(defun org-publish-org-rss (project &optional sitemap-filename)
  "Create a rss of pages in set defined by PROJECT."
  (let* ((project-plist (cdr project)) ; (:property value :property value ...) ou (:components ("proj1" "proj2"...)
	 (dir (file-name-as-directory
	       (plist-get project-plist :base-directory)));on recupere le dossier a traiter
	 (exclude-regexp (plist-get project-plist :exclude)) ;s'il y a des exclusions
	 (files (nreverse (org-publish-get-base-files project exclude-regexp)));liste des fichiers du projet (sans les exclus)
	 (rss-filename (concat dir "other/rss.rss")) ;nom du fichier rss
	 (sitemap-filename (concat dir (or sitemap-filename "sitemap.org"))) ;nom du fichier sitemap
	 (visiting (find-buffer-visiting rss-filename));est-ce qu'on visite le fichier "rss.org"? (contient le buffer, nil sinon)
	 file rss-buffer);fin du let*, file et rss-buffer sont nil
    (with-current-buffer (setq rss-buffer;on travaille temporairement avec ce fichier qui contient
			       (or visiting (find-file rss-filename))); soit le buffer qui le contenait, soit un nouveau buffer qu'on ouvre et qui contient "rss.org"
      (erase-buffer) ;on l'efface
      (insert (concat "<?xml version=\"1.0\" encoding=\"UTF-8\" ?>\n<rss version=\"2.0\">\n")) ; on met le debut
      (insert (concat "<channel>\n"))
      (insert (concat "<description>Flux de fredtantini.free.fr</description>\n"))
      (insert (concat "<link>http://fredtantini.free.fr</link>\n"))
      (insert (concat "<title>Flux de fredtantini.free.fr</title>\n"))
      (while (setq file (pop files)) ; tant qu'il y a des fichiers dans le projet à traiter
	(let ((link (file-relative-name file dir))) ; le nom relatif par rapport au dossier à traiter, lnk
	  ;; rss shouldn't list sitemap and indexes
          (message (file-truename sitemap-filename))
          (message (file-truename "blog.org"))
          (message (file-truename file))
	  (unless (or (equal (file-truename sitemap-filename)
			 (file-truename file))
                      (equal (file-truename "blog.org") (file-truename file))) ;cf commentaire
            (insert (concat "<item>\n<title>"))
            (insert (concat (org-publish-find-title file)))
            (insert (concat "</title>\n<link>http://fredtantini.free.fr/"))
            (insert (concat (if (string-match "\\.org$" link)
                                (replace-match ".html" t t link)
                              )))
            (insert (concat "</link>\n<description><![CDATA["))
            (with-temp-buffer
              (insert-file-contents file nil nil nil t)
              (mark-whole-buffer)
              (org-html-convert-region-to-html)
              (copy-region-as-kill (point-min) (point-max))
              )
            (yank)
            (insert (concat "]]>\n</description>\n</item>\n"))
            ))
      (save-buffer));on sauve le fichier
      (insert (concat "</channel>\n"))
      (insert (concat "</rss>\n")) ; on met la fin
      (save-buffer)
      (or visiting (kill-buffer rss-buffer)))));et on tue le buffer si on ne le visitait pas


(defun org-publish-create-index (project &optional sitemap-filename)
  "Create a rss of pages in set defined by PROJECT."
  (let* ((project-plist (cdr project)) ; (:property value :property value ...) ou (:components ("proj1" "proj2"...)
	 (dir (file-name-as-directory
	       (plist-get project-plist :base-directory)));on recupere le dossier a traiter
	 (exclude-regexp (plist-get project-plist :exclude)) ;s'il y a des exclusions
	 (files (nreverse (org-publish-get-base-files project exclude-regexp)));liste des fichiers du projet (sans les exclus)
	 (rss-filename (concat dir "other/rss.rss")) ;nom du fichier rss
	 (sitemap-filename (concat dir (or sitemap-filename "sitemap.org"))) ;nom du fichier sitemap
	 (blog-filename (concat dir "blog.org")) ;nom du fichier index du blog
	 (visiting (find-buffer-visiting blog-filename));est-ce qu'on visite le fichier
	 file blog-buffer);fin du let*, file et rss-buffer sont nil
    (with-current-buffer (setq blog-buffer;on travaille temporairement avec ce fichier qui contient
			       (or visiting (find-file blog-filename))); soit le buffer qui le contenait, soit un nouveau buffer qu'on ouvre et qui contient "blog.org"
      (erase-buffer) ;on l'efface
      (insert (concat "#+TITLE: Articles du blog\n")) ; on met le debut
      (while (setq file (pop files)) ; tant qu'il y a des fichiers dans le projet à traiter
	(let ((link (file-relative-name file dir))) ; le nom relatif par rapport au dossier à traiter, lnk
	  ;; rss shouldn't list sitemap/rss/index
	  (unless (equal (file-truename blog-filename)
			 (file-truename file)) ;cf commentaire
            (insert "* [[")
            (insert "http://fredtantini.free.fr/")
            (insert (concat (if (string-match "\\.org$" link)
                                (replace-match ".html" t t link)
                              )))
            (insert "][")
            (insert (concat (org-publish-find-title file) "]]\n"))

            (insert "#+INCLUDE: \"")
            (insert (file-truename file))
            (insert "\" :lines \"3-\"\n")
            ))
      (save-buffer));on sauve le fichier
      (save-buffer)
      (or visiting (kill-buffer blog-buffer)))));et on tue le buffer si on ne le visitait pas


(defun org-publish-org-rss-and-sitemap (project &optional sitemap-filename)
  "appel a rss et sitemap"
  (org-publish-org-rss project)
  (org-publish-create-index project)
  (org-publish-org-sitemap project sitemap-filename))

