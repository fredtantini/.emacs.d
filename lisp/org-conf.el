
(defvar org-emphasis-regexp-components
  '(" \t ’('\"{" "- \t ….,:!?;'\")}\\" " \t\r\n,'" "." 1)
  "support for ' ', '…' and '’'")

(defface my-face-org-keystroke
  '((t (:inherit shadow 
        :box (:line-width -2 ;neg. in order to keep the same size of lines
              :color "grey75"
              :style pressed-button)))) "Face for keystrokes"
        :group 'org-faces)


(require 'ox) 
;;otherwise org-element--parse-objects: Symbol's function definition is void: org-element-my-object-keystroke-parser

(defun org-html-keystroke (keystroke contents info)
  "Transcode KEYSTROKE from Org to HTML.
CONTENTS is nil.  INFO is a plist holding contextual
information."
  (format (or (cdr (assq 'my-object-keystroke org-html-text-markup-alist)) "%s")
	  (org-html-encode-plain-text (org-element-property :value keystroke))))


(defconst org-element-object-successor-alist
  '((subscript . sub/superscript) (superscript . sub/superscript)
    (bold . text-markup) (code . text-markup) (italic . text-markup)
    (strike-through . text-markup) (underline . text-markup)
    (verbatim . text-markup) (entity . latex-or-entity)
    (latex-fragment . latex-or-entity) (my-object-keystroke . text-markup))
  "Alist of translations between object type and successor name.
Sharing the same successor comes handy when, for example, the
regexp matching one object can also match the other object.")

(defconst org-element-all-objects
  '(bold code entity export-snippet footnote-reference inline-babel-call
	 inline-src-block italic line-break latex-fragment link macro
	 radio-target statistics-cookie strike-through subscript superscript
	 table-cell target timestamp underline verbatim my-object-keystroke)
  "Complete list of object types.")



(defun org-element-text-markup-successor ()
  "Search for the next text-markup object.

Return value is a cons cell whose CAR is a symbol among `bold',
`italic', `underline', `strike-through', `code' and `verbatim'
and CDR is beginning position."
  (save-excursion
    (unless (bolp) (backward-char))
    (when (re-search-forward org-emph-re nil t)
      (let ((marker (match-string 3)))
	(cons (cond
	       ((equal marker "*") 'bold)
	       ((equal marker "/") 'italic)
	       ((equal marker "_") 'underline)
	       ((equal marker "+") 'strike-through)
	       ((equal marker "~") 'code)
	       ((equal marker "=") 'verbatim)
	       ((equal marker "‰") 'my-object-keystroke) ;a ajouter
	       (t (error "Unknown marker at %d" (match-beginning 3))))
	      (match-beginning 2))))))



(defun org-element-my-object-keystroke-parser ()
  "Parse code object at point.

Return a list whose CAR is `my-object-keystroke' and CDR is a plist with
`:value', `:begin', `:end' and `:post-blank' keywords.

Assume point is at the first tilde marker."
  (interactive)
  (save-excursion
    (unless (bolp) (backward-char 1))
    (looking-at org-emph-re)
    (let ((begin (match-beginning 2))
	  (value (org-match-string-no-properties 4))
	  (post-blank (progn (goto-char (match-end 2))
			     (skip-chars-forward " \t")))
	  (end (point)))
      (list 'my-object-keystroke
	    (list :value value
		  :begin begin
		  :end end
		  :post-blank post-blank)))))


(defun org-element-my-object-keystroke-interpreter (keystroke contents)
  "Interpret KEYSTROKE object as Org syntax.
CONTENTS is nil."
  (format "‰%s‰" (org-element-property :value keystroke)))

;;Export
(org-export-define-derived-backend 'my-html 'html
  :translate-alist '((my-object-keystroke . org-html-keystroke))
  :menu-entry ' (?h 1
                    ((?r "my-html"  org-html-export-to-my-html)
                     (?b "as-html"  org-html-export-as-my-html)
                     )))

(defun org-html-export-to-my-html
  (&optional async subtreep visible-only body-only ext-plist)
  "Export current buffer to a HTML file.

Return output file's name."
  (interactive)
  (let* ((extension (concat "." org-html-extension))
	 (file (org-export-output-file-name extension subtreep))
	 (org-export-coding-system org-html-coding-system))
    (org-export-to-file 'my-html file
      async subtreep visible-only body-only ext-plist)))


(defun org-html-publish-to-my-html (plist filename pub-dir)
  "Publish an org file to my-html.
Return output file name."
  (org-publish-org-to 'my-html filename
		      (concat "." (or (plist-get plist :html-extension)
				      org-html-extension "html"))
		      plist pub-dir))
(defun org-html-export-as-my-html
  (&optional async subtreep visible-only body-only ext-plist)
  "Export current buffer to an my-html buffer."
  (interactive)
  (org-export-to-buffer 'my-html "*Org my-HTML Export*"
    async subtreep visible-only body-only ext-plist
    (lambda () (set-auto-mode t))))

(defun org-html-convert-region-to-my-html ()
  "Assume the current region has org-mode syntax, and convert it to HTML.
This can be used in any buffer.  For example, you can write an
itemized list in org-mode syntax in an HTML buffer and use this
command to convert it."
  (interactive)
  (org-export-replace-region-by 'my-html))


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;(require 'ox-rss) 


;---------------------------------------------------------------------
; org-mode
;---------------------------------------------------------------------

(setq posts-by-page 10)
(setq rss-max-item 50)
(require 'cl) ;pour subseq
(defun org-publish-create-rss (project)
  "Create a rss of pages in set defined by PROJECT."
  (let* ((project-plist (cdr project)) ; (:property value :property value ...) ou (:components ("proj1" "proj2"...)
	 (dir (file-name-as-directory
	       (plist-get project-plist :base-directory)));on recupere le dossier a traiter
	 (exclude-regexp (plist-get project-plist :exclude)) ;s'il y a des exclusions
	 (files (subseq (nreverse (org-publish-get-base-files project exclude-regexp)) 0 (1+ rss-max-item)));liste des fichiers du projet (sans les exclus)
	 (rss-filename (concat dir "other/rss.rss")) ;nom du fichier rss
	 (sitemap-filename (concat dir (or (plist-get project-plist :sitemap-filename) "sitemap.org"))) ;nom du fichier sitemap
	 (visiting (find-buffer-visiting rss-filename));est-ce qu'on visite le fichier "rss.org"? (contient le buffer, nil sinon)
	 file rss-buffer);fin du let*, file et rss-buffer sont nil
    (with-current-buffer (setq rss-buffer;on travaille temporairement avec ce fichier qui contient
			       (or visiting (find-file rss-filename))); soit le buffer qui le contenait, soit un nouveau buffer qu'on ouvre et qui contient "rss.org"
      (erase-buffer) ;on l'efface
      (insert
        (concat "<?xml version=\"1.0\" encoding=\"UTF-8\" ?>\n<rss version=\"2.0\">\n" ; on met le debut
                "<channel>\n"
                "<description>Flux de fredtantini.free.fr</description>\n"
                "<link>http://fredtantini.free.fr</link>\n"
                "<title>Flux de fredtantini.free.fr</title>\n"))
      (while (setq file (pop files)) ; tant qu'il y a des fichiers dans le projet à traiter
	(let ((link (file-relative-name file dir))) ; le nom relatif par rapport au dossier à traiter, lnk
	  ;; rss shouldn't list sitemap
          (message (file-truename file))
	  (unless (or (string-match sitemap-filename (file-truename file))
                      (string-match "blog-[0-9]+.org" (file-truename file))) ;cf commentaire
            (insert (concat "<item>\n<title>"
                            (org-publish-find-title file)
                            "</title>\n<link>http://fredtantini.free.fr/"
                            (if (string-match "\\.org$" link)
                                (replace-match ".html" t t link)
                              )
                            "</link>\n<description><![CDATA["))
            (with-temp-buffer
              (insert-file-contents file nil nil nil t)
              (mark-whole-buffer)
              (org-html-convert-region-to-my-html)
              (copy-region-as-kill (point-min) (point-max))
              )
            (yank)
            (insert  "]]>\n</description>\n</item>\n")
            ))
      (save-buffer));on sauve le fichier
      (insert (concat "</channel>\n"
                      "</rss>\n")) ; on met la fin
      (save-buffer)
      (or visiting (kill-buffer rss-buffer)))));et on tue le buffer si on ne le visitait pas


(defun org-publish-create-index (project)
  "Create a rss of pages in set defined by PROJECT."
  (let* ((project-plist (cdr project)) ; (:property value :property value ...) ou (:components ("proj1" "proj2"...)
	 (dir (file-name-as-directory
	       (plist-get project-plist :base-directory)));on recupere le dossier a traiter
	 (exclude-regexp (plist-get project-plist :exclude)) ;s'il y a des exclusions
	 (files (nreverse (org-publish-get-base-files project exclude-regexp)));liste des fichiers du projet (sans les exclus)
	 (sitemap-filename (concat dir (or (plist-get project-plist :sitemap-filename) "sitemap.org"))) ;nom du fichier sitemap
         (current-post 0)
         (current-page 0)
         file blog-pagename blog-buffer)
    (while (setq file (pop files)) ; tant qu'il y a des fichiers dans le projet à traiter
      (unless (or (string-match sitemap-filename (file-truename file))
                  (string-match "blog-[0-9]+.org" (file-truename file))) ;cf commentaire
        ;on met à jour le nb de page, de post, le fichier .org sur lequel écrire, etc.
        (setq current-page (1+ (/ current-post posts-by-page)))
        (setq current-post (1+ current-post))
        (setq blog-pagename (concat dir "blog-" (number-to-string current-page) ".org"))
        (setq visiting (find-buffer-visiting blog-pagename))
        (with-current-buffer (setq blog-buffer
			       (or visiting (find-file blog-pagename)))
        ;si c'est le premier de la page
          (if (= (% current-post posts-by-page) 1)
              (progn
                (erase-buffer) ;on efface au cas où il existait déjà
                (insert (concat "#+TITLE: Articles du blog - Page "
                                (number-to-string current-page)
                                "\n" ; on met le titre
;                                "#+OPTIONS: f:nil\n"
                                ))
                )
            )

          ;dans tous les cas, on met titre avec lien puis contenu
          (let ((link (file-relative-name file dir))) ; le nom relatif par rapport au dossier à traiter, lnk
            (insert (concat
                    "* [[http://fredtantini.free.fr/"
                    (if (string-match "\\.org$" link)
                                (replace-match ".html" t t link)
                      )
                    "]["
                    (org-publish-find-title file)
                    "]]\n"))
            
            (insert "#+INCLUDE: \"")
            (insert (file-truename file))
            (insert "\" :lines \"3-\"\n")
            )
          (save-buffer));on sauve le fichier
        (unless visiting (kill-buffer blog-buffer)); on le tue si on ne le visitait pas
       ))
    ))

(defun org-publish-rss-index ()
    "pour creer un fichier rss et des 'index'"
    (org-publish-create-index project)
    (org-publish-prev-next project)
    (org-publish-create-rss project))


(defun org-publish-prev-next (project)
  "pour mettre les liens précédents et suivants sur les pages blog*"

  (let* ((project-plist (cdr project)) ; (:property value :property value ...) ou (:components ("proj1" "proj2"...)
	 (dir (file-name-as-directory
	       (plist-get project-plist :base-directory)));on recupere le dossier a traiter
	 (npage 0)
         (div-start "\n#+HTML:<div id=\"nav-link\">\n")
         (div-end "\n#+HTML:</div>\n")
         org-publish-sitemap-requested org-publish-temp-files
         list-blog prev-link next-link first-link last-link)
    (org-publish-get-base-files-1 dir nil "blog-[0-9]+.org$")
    (setq list-blog (reverse org-publish-temp-files))
    (while (setq file (pop list-blog)) ; tant qu'il y a des fichiers blog
      (setq visiting (find-buffer-visiting file))
      (with-current-buffer (setq blog-buffer
                                 (or visiting (find-file file)))
        (org-footnote-renumber-fn:N)
        (setq npage (1+ npage))
        (setq blog-pagename (concat ))
        (setq prev-link (concat "[[http://fredtantini.free.fr/"
                                "blog-" (number-to-string (1- npage)) ".html"
                                "][<]]"))
        (setq next-link (concat "[[http://fredtantini.free.fr/"
                                "blog-" (number-to-string (1+ npage)) ".html"
                                "][>]]"))
        (setq first-link (concat "[[http://fredtantini.free.fr/"
                                "blog-1.html"
                                "][«]]"))
        (setq last-link (concat "[[http://fredtantini.free.fr/"
                                "blog-" (number-to-string (length org-publish-temp-files)) ".html"
                                "][»]]"))

        (insert div-start)
        (if (> npage 1)
            (progn
              (goto-char (point-max))
              (insert (concat
                       first-link
                       " "
                       prev-link))
              )
          )
        (insert " ")
        (if list-blog
            (progn
              (goto-char (point-max))
              (insert (concat
                       next-link
                       " "
                       last-link))
              )
          )
        (insert div-end)
        (save-buffer)
        )
      (or visiting (kill-buffer blog-buffer))
      )
    )
  )


;;org-export/publish
(setq org-publish-project-alist
      '(("larticles"
	 :base-directory "/data/Donnees/Sites/fredtantini/"
         :base-extension "org"
	 :publishing-directory "/var/www/FT/"
         :publishing-function (org-html-publish-to-my-html)
         :auto-sitemap t
         :sitemap-function org-publish-org-sitemap
         :sitemap-filename "sitemap.org"
         :sitemap-title "Archives"
         :sitemap-style tree
         :recursive t
         :html-head-extra
         "<link href= \"http://fredtantini.free.fr/other/mystyle.css\" rel=\"stylesheet\" type=\"text/css\" />
          <meta http-equiv=\"content-type\" content=\"application/xhtml+xml; charset=UTF-8\" />"

         :exclude "^00\\|^404.org$"
         :preparation-function (org-publish-rss-index)
	 )
	
	("limages"
	 :base-directory "/data/Donnees/Sites/fredtantini/"
	 :base-extension "jpg\\|gif\\|png"
         :publishing-directory "/var/www/FT/"
	 :publishing-function org-publish-attachment
	 :recursive t)
	
	("lother"
	 :base-directory "/data/Donnees/Sites/fredtantini/other"
	 :base-extension "css\\|el\\|rss"
         :publishing-directory "/var/www/FT/other/"
	 :publishing-function org-publish-attachment)

        ("rarticles"
         :base-directory "/data/Donnees/Sites/fredtantini/"
	 :base-extension "org"
	 :publishing-directory "/ftp:fredtantini@fredtantini.free.fr:~/"
	 :publishing-function (org-html-publish-to-my-html org-org-publish-to-org)
	 :plain-source t
	 :htmlized-source t
	 :auto-sitemap t
         :sitemap-function org-publish-org-sitemap 
	 :sitemap-filename "sitemap.org"
	 :sitemap-title "Archives"
	 :sitemap-style tree
	 :recursive t
         :html-head-extra
         "<link rel=\"alternate\" type=\"appliation/rss+xml\"
                href=\"http://fredtantini.free.fr/other/rss.rss\"
                title=\"RSS feed for fredtantini.free.fr\">
          <link href= \"http://fredtantini.free.fr/other/mystyle.css\" rel=\"stylesheet\" type=\"text/css\" />
          <meta http-equiv=\"content-type\" content=\"application/xhtml+xml; charset=UTF-8\" />
          <meta name=\"viewport\" content=\"initial-scale=1,width=device-width,minimum-scale=1\">
          <script type=\"text/javascript\">
    function rpl(expr,a,b) {
      var i=0
      while (i!=-1) {
         i=expr.indexOf(a,i);
         if (i>=0) {
            expr=expr.substring(0,i)+b+expr.substring(i+a.length);
            i+=b.length;
         }
      }
      return expr
    }

    function show_org_source(){
       document.location.href = rpl(document.location.href,\"html\",\"org\");
    }
    function show_htmlized_source(){
       document.location.href = rpl(document.location.href,\"html\",\"org.html\");
    }
</script>"
         :html-postamble "<div id=\"Archives\"><a href=\"/sitemap.html\">Autres billets</a></div><p class=\"date\">Date: %d</p><p class=\"creator\">Generated by %c - <a href=\"#\" onclick='show_org_source()'>Show Org source</a> (<a href=\"#\" onclick='show_htmlized_source()'>htmlized</a>)</p>
<p class=\"style\">CSS inspired by <a href=\"http://tontof.net/\">Tontof</a>, colors by <a href=\"http://http://theme.wordpress.com/themes/chaoticsoul/\">Chaotic Soul</a></p>
<p class=\"xhtml-validation\">%v</p>
"
         :exclude "^00\\|^404.org$"
         :preparation-function (org-publish-rss-index)
	 )
		
	("rimages"
	 :base-directory "/data/Donnees/Sites/fredtantini/"
	 :base-extension "jpg\\|gif\\|png"
	 :publishing-directory "/ftp:fredtantini@fredtantini.free.fr:~/"
	 :publishing-function org-publish-attachment
	 :recursive t
	 )
	
	("rother"
	 :base-directory "/data/Donnees/Sites/fredtantini/other/"
	 :base-extension "css\\|el\\|rss"
	 :publishing-directory "/ftp:fredtantini@fredtantini.free.fr:~/other/"
	 :publishing-function org-publish-attachment)
	

	("lft" :components ("larticles" "limages" "lother"))
	("rft" :components ("rarticles" "rimages" "rother"))
	
	("all" :components ("lft" "rft")))
      )

