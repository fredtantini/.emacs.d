(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(TeX-command-Show "View PS")
 '(TeX-command-list (quote (("TeX" "tex '\\nonstopmode\\input %t'" TeX-run-TeX nil t) ("TeX Interactive" "tex %t" TeX-run-interactive nil t) ("LaTeX" "%l '\\nonstopmode\\input{%t}'" TeX-run-LaTeX nil "nil") ("LaTeX Interactive" "%l %t" TeX-run-interactive nil t) ("LaTeX2e" "latex2e '\\nonstopmode\\input{%t}'" TeX-run-LaTeX nil t) ("View PS" "gv --noresize %f" TeX-run-silent nil "nil") ("View" "%V " TeX-run-silent nil nil) ("Print" "%p %r " TeX-run-command t nil) ("Queue" "%q" TeX-run-background nil nil) ("File" "%(o?)dvips %d -o %f" TeX-run-command nil nil) ("BibTeX" "bibtex %s" TeX-run-BibTeX nil nil) ("Index" "makeindex %s" TeX-run-command nil t) ("Check" "lacheck %s" TeX-run-compile nil t) ("Spell" "<ignored>" TeX-run-ispell-on-document nil nil) ("Other" "" TeX-run-command t t) ("Makeinfo" "makeinfo %t" TeX-run-compile nil t) ("Makeinfo HTML" "makeinfo --html %t" TeX-run-compile nil t) ("AmSTeX" "amstex '\\nonstopmode\\input %t'" TeX-run-TeX nil t))))
 '(auto-compression-mode t nil nil "Pour voir les fichiers tar/gz")
 '(auto-save-list-file-prefix "~/.emacs.d/config/auto-save-list/.saves-")
 '(case-replace t nil nil "remplace sans conversion de casse")
 '(colon-double-space nil nil nil "pas d'espace double après les deux-points")
 '(column-number-mode t nil nil "numero de colonne")
 '(current-language-environment "UTF-8" nil nil "   was Latin-9")
 '(custom-enabled-themes (quote (zenburn)))
 '(custom-safe-themes (quote ("3cc6c42bee60b0a031be65497e630b3ffaaaa41ffc1f41b9f7863ee484688640" default)))
 '(custom-theme-load-path (quote (custom-theme-directory t "/home/fred/.emacs.d/config")))
 '(default-input-method "utf-8" nil nil "was latin-9-prefix")
 '(desktop-base-file-name "desktop" nil nil "ou enregistrer le fichier .emacs.desktop")
 '(desktop-base-lock-name "desktop.lock" nil nil "ou enregistrer le fichier .emacs.desktop.lock")
 '(desktop-buffers-not-to-save nil nil nil "exemple de buffers à ne pas sauver : \\(^nn\\.a[0-9]+\\|\\.log\\|(ftp)\\|^tags\\|^TAGS\\|\\.emacs.*\\|\\.diary\\|\\.newsrc-dribble\\|\\.bbdb\\)$")
 '(desktop-globals-to-save (quote (desktop-missing-file-warning tags-file-name tags-table-list search-ring regexp-search-ring register-alist (read-command-history . 9) (file-name-history . 9) (command-history . 9) (grep-history . 9) (compile-history . 9) (minibuffer-history . 9) (hypropos-regexp-history . 9) desktop-globals-to-save)) nil nil "les variables globales à sauver à chaque session. (x . n) = tronquer x à n éléments.")
 '(desktop-load-locked-desktop (quote ask) nil nil "si un .lock est trouvé, demander quoi faire")
 '(desktop-path (quote ("~/.emacs.d/config/")) nil nil "liste des dossiers ou chercher le fichier desktop")
 '(desktop-save t nil nil "toujours sauver les sessions")
 '(desktop-save-mode t nil nil "on sauve les sessions")
 '(history-length 300)
 '(indent-tabs-mode nil)
 '(inhibit-default-init nil nil nil "ne pas charger la bibliotheque default")
 '(inhibit-startup-screen nil nil nil "pas de startup screen")
 '(initial-scratch-message nil)
 '(keyboard-coding-system (quote utf-8-unix))
 '(max-specpdl-size 1000 nil nil "limite sur le nombre de variables lisp")
 '(message-log-max 1000)
 '(org-agenda-files nil)
 '(org-agenda-include-diary t)
 '(org-emphasis-alist (quote (("*" bold) ("/" italic) ("_" underline) ("=" org-code verbatim) ("~" org-verbatim verbatim) ("+" (:strike-through t)) ("‰" my-face-org-keystroke verbatim))))
 '(org-enforce-todo-dependencies t nil nil "parent can be DONE once all children are DONE")
 '(org-export-author-info nil)
 '(org-export-creator-info nil)
 '(org-export-headline-levels 6)
 '(org-export-html-home/up-format "<div id=\"org-div-home-and-up\" style=\"text-align:right;font-size:70%%;white-space:nowrap;\">
 <a accesskey=\"h\" href=\"http://fredtantini.free.fr/%s\"> index </a>
 |
 <a accesskey=\"H\" href=\"http://fredtantini.free.fr/%s\"> archives </a>
</div>")
 '(org-export-html-link-home "index.html")
 '(org-export-html-link-up "sitemap.html")
 '(org-export-html-postamble nil)
 '(org-export-html-preamble-format (quote (("en" "<script type=\"text/javascript\">
    function rpl(expr,a,b) {
      var i=0
      while (i!=-1) {
         i=expr.indexOf(a,i);
         if (i>=0) {
            expr=expr.substring(0,i)+b+expr.substring(i+a.length);
            i+=b.length;
         }
      }
      return expr
    }

    function show_org_source(){
       document.location.href = rpl(document.location.href,\"html\",\"org\");
    }
    function show_htmlized_source(){
       document.location.href = rpl(document.location.href,\"html\",\"org.html\");
    }
</script>"))))
 '(org-export-html-style "<link rel=\"stylesheet\" type=\"text/css\" href=\"http://fredtantini.free.fr/other/mystyle.css\" />")
 '(org-export-html-style-include-default nil)
 '(org-export-section-number-format (quote ((("1" ".")) . ". -")))
 '(org-export-with-section-numbers nil)
 '(org-export-with-toc nil)
 '(org-html-postamble nil)
 '(org-html-text-markup-alist (quote ((bold . "<b>%s</b>") (code . "<code>%s</code>") (italic . "<i>%s</i>") (strike-through . "<del>%s</del>") (underline . "<span class=\"underline\">%s</span>") (verbatim . "<code>%s</code>") (my-object-keystroke . "<kbd>%s</kbd>"))))
 '(org-log-done (quote time) nil nil "quand on passe en DONE, timestamp")
 '(org-publish-sitemap-sort-files (quote anti-chronogically))
 '(org-publish-timestamp-directory "~/.config/org-timestamps/")
 '(org-src-fontify-natively t)
 '(org-use-sub-superscripts nil)
 '(remember-data-file "/data/Donnees/Org/notes.org")
 '(save-place t nil (saveplace) "sauve l'emplacement du point dans le fichier")
 '(save-place-file "~/.emacs.d/places" nil nil "ou mettre le fichier pour save-place-alist")
 '(scroll-bar-mode nil)
 '(selection-coding-system nil nil nil "was iso-8859-15")
 '(sentence-end-double-space nil nil nil "une seule espace pour terminer les phrases")
 '(shell-file-name "/bin/bash" nil nil "inferior shells")
 '(show-paren-mode t nil nil "pairage des parenthèses")
 '(show-paren-style (quote parenthesis) nil nil "show the matching paren")
 '(speedbar-directory-unshown-regexp "^\\(CVS\\|RCS\\|\\.\\|SCCS\\)")
 '(tool-bar-mode nil)
 '(transient-mark-mode t nil nil "highlight de la région")
 '(visible-bell t))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 )
